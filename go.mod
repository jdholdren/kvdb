module gitlab.com/jdholdren/kvdb

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/peterbourgon/ff v1.7.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/twitchtv/twirp v8.0.0+incompatible
	google.golang.org/protobuf v1.26.0
)
