.PHONY: protos.gen
protos.gen:
	protoc --twirp_out=. --go_out=. ./api/kvdbserver/kvdbserver.proto

.PHONY: build
build:
	@echo "Made stuff"

.PHONY: build.apiserver
build.apiserver:
	mkdir -p ./built
	CGO_ENABLED=0 go build -ldflags '-w' -o ./built/kvdb ./cmd/apiserver
