// Starts the API server for the kv database
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/jdholdren/kvdb/api/kvdbserver"
	"gitlab.com/jdholdren/kvdb/internal/apiserver"

	"github.com/peterbourgon/ff"
)

func main() {
	ctx, done := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer done()

	if err := realMain(ctx); err != nil {
		log.Fatalf("error serving: %s", err)
	}
}

func realMain(ctx context.Context) error {
	fs := flag.NewFlagSet("kvdb", flag.ExitOnError)
	var (
		port = fs.Int("port", 4444, "Port used for serving the api")
	)
	_ = ff.Parse(fs, os.Args[1:])

	// TODO: Do further dependency setup
	srv := &apiserver.Server{}

	// Make an instance of the http server
	kvs := kvdbserver.NewKVServer(srv)

	// Start running the server
	srvErr := make(chan error, 1)
	go func() {
		err := http.ListenAndServe(fmt.Sprintf(":%d", *port), kvs)
		if err != nil && err != http.ErrServerClosed {
			srvErr <- err
		}
	}()

	log.Printf("Listening on port: %d", *port)

	select {
	case <-ctx.Done(): // Shutdown signal from the user
	case err := <-srvErr:
		return err
	}

	return nil
}
