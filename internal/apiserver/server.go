package apiserver

import (
	"context"

	"github.com/twitchtv/twirp"
	"gitlab.com/jdholdren/kvdb/api/kvdbserver"
)

// Server holds the dependencies for the apiserver
type Server struct {
}

func (s *Server) Get(context.Context, *kvdbserver.GetRequest) (*kvdbserver.Record, error) {
	// TODO
	return nil, twirp.NewError(twirp.Unimplemented, "unimplemented")
}

func (s *Server) Set(context.Context, *kvdbserver.Record) (*kvdbserver.Record, error) {
	// TODO
	return nil, twirp.NewError(twirp.Unimplemented, "unimplemented")
}
