# kvdb

A small, distributed key-value database.

# Contributing

## Generating Protobufs

To generate protobufs, run `make protos.gen`.
